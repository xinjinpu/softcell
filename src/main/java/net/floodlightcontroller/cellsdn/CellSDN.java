/**
 *	Copyright 2012, Xin Jin, Princeton University
 **/

/*
 * this is the runtime of CellSDN
 * now supports mobility management, forwarding, monitor
 */


package net.floodlightcontroller.cellsdn;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;

import org.openflow.protocol.OFFlowRemoved;
import org.openflow.protocol.OFMatch;
import org.openflow.protocol.OFMessage;
import org.openflow.protocol.OFPacketIn;
import org.openflow.protocol.OFType;
import org.openflow.util.HexString;

import net.floodlightcontroller.core.FloodlightContext;
import net.floodlightcontroller.core.IFloodlightProviderService;
import net.floodlightcontroller.core.IOFMessageListener;
import net.floodlightcontroller.core.IOFSwitch;
import net.floodlightcontroller.core.module.FloodlightModuleContext;
import net.floodlightcontroller.core.module.FloodlightModuleException;
import net.floodlightcontroller.core.module.IFloodlightModule;
import net.floodlightcontroller.core.module.IFloodlightService;
import net.floodlightcontroller.devicemanager.SwitchPort;
import net.floodlightcontroller.packet.Ethernet;
import net.floodlightcontroller.routing.IRoutingService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CellSDN implements IOFMessageListener, IFloodlightModule {

	protected IFloodlightProviderService floodlight_provider;
	protected IRoutingService routing_engine;
	protected HostSwitchMap host_switch_map;
	protected PortChangeList port_change_list;
	protected CellRouting cell_routing;
	
	protected static Logger logger;

	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return "cellsdn";
	}

	@Override
	public boolean isCallbackOrderingPrereq(OFType type, String name) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean isCallbackOrderingPostreq(OFType type, String name) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public Collection<Class<? extends IFloodlightService>> getModuleServices() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Map<Class<? extends IFloodlightService>, IFloodlightService> getServiceImpls() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Collection<Class<? extends IFloodlightService>> getModuleDependencies() {
		Collection<Class<? extends IFloodlightService>> l =
				new ArrayList<Class<? extends IFloodlightService>>();
		l.add(IFloodlightProviderService.class);
		return l;
	}

	@Override
	public void init(FloodlightModuleContext context)
			throws FloodlightModuleException {
		floodlight_provider = context.getServiceImpl(IFloodlightProviderService.class);
		routing_engine = context.getServiceImpl(IRoutingService.class);
		host_switch_map = new HostSwitchMap();
		port_change_list = new PortChangeList();
		cell_routing = new CellRouting();
		logger = LoggerFactory.getLogger(CellSDN.class);
	}

	@Override
	public void startUp(FloodlightModuleContext context) {
		floodlight_provider.addOFMessageListener(OFType.PACKET_IN, this);
		floodlight_provider.addOFMessageListener(OFType.FLOW_REMOVED, this);
		floodlight_provider.addOFMessageListener(OFType.ERROR, this);
	}

	// this function composes various modules
	@Override
	public net.floodlightcontroller.core.IListener.Command receive(
			IOFSwitch sw, OFMessage msg, FloodlightContext cntx) {
		switch (msg.getType()) {
			case PACKET_IN:
				return this.processPacketInMessage(sw, (OFPacketIn)msg, cntx);
			case FLOW_REMOVED:
				return this.processFlowRemovedMessage(sw, (OFFlowRemoved)msg);
			case ERROR:
				return Command.CONTINUE;
			default:
				break;
		}
		logger.error("received an unexpected message {} from switch {}", msg, sw);
		return Command.CONTINUE;
	}
	
	/*
	 * TODO
	 */
	private Command processPacketInMessage(IOFSwitch sw, OFPacketIn pi, FloodlightContext cntx) {
		
		// TODO: temporarily set do_flush
		boolean do_flush = true;
		
		// load packet header to OFMatch
		OFMatch match = new OFMatch();
		match.loadFromPacket(pi.getPacketData(), pi.getInPort());
		
		logger.info("packet type {}", new Object [] {HexString.toHexString(match.getDataLayerType())});
		/*
		 * logger.info("packet type {}", new Object [] {HexString.toHexString(match.getDataLayerType())});
		 * we're only interested in ARP (0x0806) and IPv4 (0x0800) packet
		 */
		short data_layer_type = match.getDataLayerType();
		if ( data_layer_type != Ethernet.TYPE_ARP && data_layer_type != Ethernet.TYPE_IPv4) {
			return Command.CONTINUE;
		}
		
		Long src_mac = Ethernet.toLong(match.getDataLayerSource());
		Long dst_mac = Ethernet.toLong(match.getDataLayerDestination());
		Short vlan = match.getDataLayerVirtualLan();
		SwitchPort cur_attach_point = new SwitchPort(sw.getId(), pi.getInPort());
		
		/* copy from LearningSwitch.java, don't know the meaning... currently disable it
		if ((dest_mac & 0xfffffffffff0L) == 0x0180c2000000L) {
			if (logger.isTraceEnabled()) {
				logger.trace("ignoring packet addressed to 802.1D/Q reserved addr: switch {} vlan {} dest MAC {}",
						new Object[]{ sw, vlan, HexString.toHexString(dest_mac) });
			}
			return Command.STOP;
		}*/
		
		/*
		 * if attach_point is null, it's a new host
		 * otherwise
		 */
		SwitchPort src_attach_point = host_switch_map.getSwitchPort(src_mac);
		SwitchPort dst_attach_point = host_switch_map.getSwitchPort(dst_mac);
		if (src_attach_point == null) {
			/*
			 * it's a new host
			 * add to host_switch_map
			 * set up path to dst_mac, if not found, flood
			 */
			logger.info("1: src_attach_point == null");
			host_switch_map.putMap(src_mac, cur_attach_point);
			if (dst_attach_point == null) {
				cell_routing.doFlood(sw, pi, floodlight_provider, logger);
			}
			else {
				cell_routing.doRouting(src_mac, dst_mac, cur_attach_point, dst_attach_point, routing_engine,
						floodlight_provider, logger, match, sw, pi, do_flush, cntx);
			}
		}
		else if (src_attach_point.equals(cur_attach_point)) {
			/*
			 * it's an old host, the same attach point
			 * set up path to dst_mac, if not found, flood
			 */
			logger.info("2: src_attach_point.equals(cur_attach_point)");
			if (dst_attach_point == null) {
				cell_routing.doFlood(sw, pi, floodlight_provider, logger);
			}
			else {
				cell_routing.doRouting(src_mac, dst_mac, cur_attach_point, dst_attach_point, routing_engine,
						floodlight_provider, logger, match, sw, pi, do_flush, cntx);
			}
		}
		else if (port_change_list.containSwitchPort(cur_attach_point)) {
			/*
			 * it's an old host, found in recent port change, means it moved
			 * clean switch_port_change
			 * update host_switch_map
			 * set up path to dst_mac, if not found, flood
			 * set up tunnel
			 */
			logger.info("3: port_change_list.containSwitchPort(cur_attach_point)");
			port_change_list.cleanSwitchPort(cur_attach_point);
			host_switch_map.updateMap(src_mac, cur_attach_point);
			if (dst_attach_point == null) {
				cell_routing.doFlood(sw, pi, floodlight_provider, logger);
			}
			else {
				cell_routing.doRouting(src_mac, dst_mac, cur_attach_point, dst_attach_point, routing_engine,
						floodlight_provider, logger, match, sw, pi, do_flush, cntx);
			}
			cell_routing.doTunnel(src_attach_point, cur_attach_point, routing_engine, floodlight_provider,
					logger, match, sw, pi, do_flush, cntx);
		}
		else {
			/*
			 * it's an old host, the SwitchPort is not the SwithPort the host moves to
			 * set up path to dst_mac, if not found, flood
			 */
			logger.info("4: others");
			if (dst_attach_point == null) {
				cell_routing.doFlood(sw, pi, floodlight_provider, logger);
			}
			else {
				cell_routing.doRouting(src_mac, dst_mac, cur_attach_point, dst_attach_point, routing_engine,
						floodlight_provider, logger, match, sw, pi, do_flush, cntx);
			}
		}
		/*else {
			/*
			 * it's an old host, different attach point
			 * update host_switch_map
			 * set up path to dst_mac, if not found, flood
			 * set up tunnel
			 */
			/*host_switch_map.updateMap(src_mac, cur_attach_point);
			if (dst_attach_point == null) {
				cell_routing.doFlood(sw, pi, floodlight_provider, logger);
			}
			else {
				cell_routing.doRouting(src_mac, dst_mac, cur_attach_point, dst_attach_point, routing_engine,
						floodlight_provider, logger, match, sw, pi, do_flush, cntx);
			}
			cell_routing.doTunnel(src_mac, cur_attach_point, src_attach_point, logger);
		}*/
		
		return Command.CONTINUE;
	}
	
	/*
	 * TODO
	 */
	private Command processFlowRemovedMessage(IOFSwitch sw, OFFlowRemoved flowRemoveMessage) {
		return Command.CONTINUE;
	}

}
