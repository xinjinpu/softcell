package net.floodlightcontroller.cellsdn;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.openflow.protocol.OFFlowMod;
import org.openflow.protocol.OFMatch;
import org.openflow.protocol.OFPacketIn;
import org.openflow.protocol.OFPacketOut;
import org.openflow.protocol.OFPort;
import org.openflow.protocol.OFType;
import org.openflow.protocol.action.OFAction;
import org.openflow.protocol.action.OFActionOutput;
import org.openflow.util.HexString;

import net.floodlightcontroller.core.FloodlightContext;
import net.floodlightcontroller.core.IFloodlightProviderService;
import net.floodlightcontroller.core.IOFSwitch;
import net.floodlightcontroller.core.util.AppCookie;
import net.floodlightcontroller.devicemanager.SwitchPort;
import net.floodlightcontroller.routing.IRoutingService;
import net.floodlightcontroller.routing.Route;
import net.floodlightcontroller.topology.NodePortTuple;

import org.slf4j.Logger;

public class CellRouting {
	
	public static int CELL_ROUTING_APP_ID = 20;
	public static short FLOWMOD_DEFAULT_IDLE_TIMEOUT = 5;
	public static short FLOWMOD_DEFAULT_HARD_TIMEOUT = 0;
	
	protected void doFlood(IOFSwitch sw,
						   OFPacketIn pi,
						   IFloodlightProviderService floodlight_provider,
						   Logger logger) {
		
		logger.info("Enter doFlooding module, flood at switch:{}",
				new Object[]{sw});
		
		OFPacketOut po = (OFPacketOut) floodlight_provider.getOFMessageFactory().getMessage(OFType.PACKET_OUT);
		
		List<OFAction> actions = new ArrayList<OFAction>();
		if (sw.hasAttribute(IOFSwitch.PROP_SUPPORTS_OFPP_FLOOD)) {
			actions.add(new OFActionOutput(OFPort.OFPP_FLOOD.getValue(), (short)0xFFFF));
		}
		else {
			actions.add(new OFActionOutput(OFPort.OFPP_ALL.getValue(), (short)0xFFFF));
		}
		po.setActions(actions);
		po.setActionsLength((short)OFActionOutput.MINIMUM_LENGTH);
		
		short po_length = (short)(po.getActionsLength() + OFPacketOut.MINIMUM_LENGTH);
		po.setBufferId(pi.getBufferId());
		po.setInPort(pi.getInPort());
		if (pi.getBufferId() == OFPacketOut.BUFFER_ID_NONE) {
			byte[] packet_data = pi.getPacketData();
			po_length += packet_data.length;
			po.setPacketData(packet_data);
		}
		po.setLength(po_length);
		
		try {
			sw.write(po, null);
		} catch (IOException e) {
			logger.error("Failed to write {} to switch {}: {}", new Object[]{po, sw, e});
		}
	}
	
	// TODO: clean the parameters
	protected void doRouting(Long src_mac,
							 Long dst_mac,
							 SwitchPort src_attach_point,
							 SwitchPort dst_attach_point,
							 IRoutingService routing_engine,
							 IFloodlightProviderService floodlight_provider,
							 Logger logger,
							 OFMatch match,
							 IOFSwitch pi_sw,
							 OFPacketIn pi,
							 boolean do_flush,
							 FloodlightContext cntx) {
		
		logger.info("Enter doRouting module, set routing for src:{} dst:{}",
				new Object[]{HexString.toHexString(src_mac), HexString.toHexString(dst_mac)});
		Integer wildcard_hints = ((Integer)pi_sw.getAttribute(IOFSwitch.PROP_FASTWILDCARDS)).intValue()
								& ~OFMatch.OFPFW_IN_PORT
								& ~OFMatch.OFPFW_DL_VLAN
								& ~OFMatch.OFPFW_DL_SRC
								& ~OFMatch.OFPFW_DL_DST
								& ~OFMatch.OFPFW_NW_SRC_MASK
								& ~OFMatch.OFPFW_NW_DST_MASK;
		
		OFActionOutput action = new OFActionOutput();
		action.setMaxLength((short)0xffff);
		List<OFAction> actions = new ArrayList<OFAction>();
		actions.add(action);
		
		OFFlowMod fm = (OFFlowMod) floodlight_provider.getOFMessageFactory().getMessage(OFType.FLOW_MOD);
		fm.setIdleTimeout(FLOWMOD_DEFAULT_IDLE_TIMEOUT);
		fm.setHardTimeout(FLOWMOD_DEFAULT_HARD_TIMEOUT);
		fm.setBufferId(OFPacketOut.BUFFER_ID_NONE);
		fm.setCookie(AppCookie.makeCookie(CELL_ROUTING_APP_ID, 0));
		fm.setCommand(OFFlowMod.OFPFC_ADD);
		fm.setMatch(match);
		fm.setActions(actions);
		fm.setLengthU(OFFlowMod.MINIMUM_LENGTH + OFActionOutput.MINIMUM_LENGTH);
		
		Route route = routing_engine.getRoute(src_attach_point.getSwitchDPID(),
											(short)src_attach_point.getPort(),
											dst_attach_point.getSwitchDPID(),
											(short)dst_attach_point.getPort());
		List<NodePortTuple> switch_port_list = route.getPath();
		for (int index = switch_port_list.size() - 1; index > 0; index -= 2) {
			long switch_id = switch_port_list.get(index).getNodeId();
			IOFSwitch sw = floodlight_provider.getSwitches().get(switch_id);
			if (sw == null) {
				if (logger.isWarnEnabled()) {
					logger.warn("Unable to push route, switch at DPID {} not available", switch_id);
				}
				return;
			}
			
			fm.setMatch(wildcard(match, sw, wildcard_hints));
			
			/*if (index == 1) {
				// see if needs to set notification
			}*/
			
			short out_port = switch_port_list.get(index).getPortId();
			short in_port = switch_port_list.get(index-1).getPortId();
			
			fm.getMatch().setInputPort(in_port);
			((OFActionOutput)fm.getActions().get(0)).setPort(out_port);
			try {
				sw.write(fm, cntx);
				if (do_flush) {
					sw.flush();
				}
				
				if (sw.getId() == src_attach_point.getSwitchDPID()) {
					pushPacket(sw, match, pi, out_port, do_flush, floodlight_provider, logger, cntx);
				}
			} catch (IOException e) {
				logger.error("Failure writing flow mod", e);
			}
			
			try {
				fm = fm.clone();
			} catch (CloneNotSupportedException e) {
				logger.error("Failure cloning flow mod", e);
			}
		}
	}
	
	// TODO:
	protected void doTunnel(SwitchPort src_attach_point,
							SwitchPort dst_attach_point,
							IRoutingService routing_engine,
							IFloodlightProviderService floodlight_provider,
							Logger logger,
							OFMatch match,
							IOFSwitch pi_sw,
							OFPacketIn pi,
							boolean do_flush,
							FloodlightContext cntx) {
		logger.info("Enter doTunnel module, from src_switch:{} to dst_switch:{}",
				new Object[]{src_attach_point, dst_attach_point});

		byte[] dst_mac = match.getDataLayerSource();
		match.setDataLayerDestination(dst_mac);
		Integer wildcard_hints = ((Integer)pi_sw.getAttribute(IOFSwitch.PROP_FASTWILDCARDS)).intValue()
								& ~OFMatch.OFPFW_DL_DST;
		
		OFActionOutput action = new OFActionOutput();
		action.setMaxLength((short)0xffff);
		List<OFAction> actions = new ArrayList<OFAction>();
		actions.add(action);
		
		OFFlowMod fm = (OFFlowMod) floodlight_provider.getOFMessageFactory().getMessage(OFType.FLOW_MOD);
		fm.setIdleTimeout(FLOWMOD_DEFAULT_IDLE_TIMEOUT);
		fm.setHardTimeout(FLOWMOD_DEFAULT_HARD_TIMEOUT);
		fm.setBufferId(OFPacketOut.BUFFER_ID_NONE);
		fm.setCookie(AppCookie.makeCookie(CELL_ROUTING_APP_ID, 0));
		fm.setCommand(OFFlowMod.OFPFC_ADD);
		fm.setMatch(match);
		fm.setActions(actions);
		fm.setLengthU(OFFlowMod.MINIMUM_LENGTH + OFActionOutput.MINIMUM_LENGTH);
		
		Route route = routing_engine.getRoute(src_attach_point.getSwitchDPID(),
											dst_attach_point.getSwitchDPID());
		List<NodePortTuple> switch_port_list = route.getPath();
		
		// switch_port_list.size - 1: dst_switch input
		{
			int index = switch_port_list.size() - 1;
			long switch_id = switch_port_list.get(index).getNodeId();
			IOFSwitch sw = floodlight_provider.getSwitches().get(switch_id);
			if (sw == null) {
				if (logger.isWarnEnabled()) {
					logger.warn("Unable to push route, switch at DPID {} not available", switch_id);
				}
				return;
			}
			
			fm.setMatch(wildcard(match, sw, wildcard_hints));
			
			short out_port = (short)dst_attach_point.getPort();
			short in_port = switch_port_list.get(index-1).getPortId();
			
			fm.getMatch().setInputPort(in_port);
			((OFActionOutput)fm.getActions().get(0)).setPort(out_port);
			try {
				sw.write(fm, cntx);
				if (do_flush) {
					sw.flush();
				}
				
				if (sw.getId() == src_attach_point.getSwitchDPID()) {
					pushPacket(sw, match, pi, out_port, do_flush, floodlight_provider, logger, cntx);
				}
			} catch (IOException e) {
				logger.error("Failure writing flow mod", e);
			}
			
			try {
				fm = fm.clone();
			} catch (CloneNotSupportedException e) {
				logger.error("Failure cloning flow mod", e);
			}
		}
		
		for (int index = switch_port_list.size() - 1; index > 0; index -= 2) {
			long switch_id = switch_port_list.get(index).getNodeId();
			IOFSwitch sw = floodlight_provider.getSwitches().get(switch_id);
			if (sw == null) {
				if (logger.isWarnEnabled()) {
					logger.warn("Unable to push route, switch at DPID {} not available", switch_id);
				}
				return;
			}
			
			fm.setMatch(wildcard(match, sw, wildcard_hints));
			
			short out_port = switch_port_list.get(index).getPortId();
			short in_port = switch_port_list.get(index-1).getPortId();
			
			fm.getMatch().setInputPort(in_port);
			((OFActionOutput)fm.getActions().get(0)).setPort(out_port);
			try {
				sw.write(fm, cntx);
				if (do_flush) {
					sw.flush();
				}
				
				if (sw.getId() == src_attach_point.getSwitchDPID()) {
					pushPacket(sw, match, pi, out_port, do_flush, floodlight_provider, logger, cntx);
				}
			} catch (IOException e) {
				logger.error("Failure writing flow mod", e);
			}
			
			try {
				fm = fm.clone();
			} catch (CloneNotSupportedException e) {
				logger.error("Failure cloning flow mod", e);
			}
		}

		// switch_port_list.size - 1: dst_switch input
		{
			int index = 0;
			long switch_id = switch_port_list.get(index).getNodeId();
			IOFSwitch sw = floodlight_provider.getSwitches().get(switch_id);
			if (sw == null) {
				if (logger.isWarnEnabled()) {
					logger.warn(
							"Unable to push route, switch at DPID {} not available",
							switch_id);
				}
				return;
			}

			fm.setMatch(wildcard(match, sw, wildcard_hints));


			short out_port = switch_port_list.get(index).getPortId();

			//fm.getMatch().setInputPort(in_port);
			((OFActionOutput) fm.getActions().get(0)).setPort(out_port);
			try {
				sw.write(fm, cntx);
				if (do_flush) {
					sw.flush();
				}

				if (sw.getId() == src_attach_point.getSwitchDPID()) {
					pushPacket(sw, match, pi, out_port, do_flush,
							floodlight_provider, logger, cntx);
				}
			} catch (IOException e) {
				logger.error("Failure writing flow mod", e);
			}

			try {
				fm = fm.clone();
			} catch (CloneNotSupportedException e) {
				logger.error("Failure cloning flow mod", e);
			}
		}
	}
	
	protected void pushPacket(IOFSwitch sw,
							  OFMatch match,
							  OFPacketIn pi,
							  short out_port,
							  boolean do_flush,
							  IFloodlightProviderService floodlight_provider,
							  Logger logger,
							  FloodlightContext cntx) {
		if (pi == null) {
			return;
		}
		else if (pi.getInPort() == out_port) {
			logger.warn("Packet out not sent as the out_port matches in_port. {}", pi);
			return;
		}
		
		if (logger.isTraceEnabled()) {
			logger.trace("PacketOut srcSwitch={} inPort={} outPort{}",
					new Object[] {sw, pi.getInPort(), out_port});
		}
		
		OFPacketOut po = (OFPacketOut) floodlight_provider.getOFMessageFactory().getMessage(OFType.PACKET_OUT);
		List<OFAction> actions = new ArrayList<OFAction>();
		actions.add(new OFActionOutput(out_port, (short)0xffff));
		
		po.setActions(actions);
		po.setActionsLength((short) OFActionOutput.MINIMUM_LENGTH);
		
		short po_length = (short)(po.getActionsLength() + OFPacketOut.MINIMUM_LENGTH);
		po.setBufferId(pi.getBufferId());
		po.setInPort(pi.getInPort());
		if (po.getBufferId() == OFPacketOut.BUFFER_ID_NONE) {
			byte[] packet_data = pi.getPacketData();
			po_length += packet_data.length;
			po.setPacketData(packet_data);
		}
		po.setLength(po_length);
		
		try {
			sw.write(po, cntx);
		} catch (IOException e) {
			logger.error("Failure writing packet out", e);
		}
	}
	
	protected OFMatch wildcard(OFMatch match, IOFSwitch sw, Integer wildcard_hints) {
		if (wildcard_hints != null) {
			return match.clone().setWildcards(wildcard_hints.intValue());
		}
		return match.clone();
	}

}
