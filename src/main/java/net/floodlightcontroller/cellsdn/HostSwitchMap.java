package net.floodlightcontroller.cellsdn;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.openflow.protocol.OFPacketIn;

import net.floodlightcontroller.core.FloodlightContext;
import net.floodlightcontroller.core.IFloodlightProviderService;
import net.floodlightcontroller.core.IOFSwitch;
import net.floodlightcontroller.devicemanager.SwitchPort;
import net.floodlightcontroller.packet.Ethernet;

public class HostSwitchMap {
	
	/*protected Long getHost (LDUpdate ldUpdate) {
		SwitchIdentifier switch_identifier = new SwitchIdentifier(ldUpdate.getSrc(), ldUpdate.getSrcPort());
		return hostSwitchMap.get(switch_identifier);
	}
	
	protected void update (LDUpdate ldUpdate) {
		SwitchIdentifier switch_identifier = new SwitchIdentifier(ldUpdate.getSrc(), ldUpdate.getSrcPort());
		//hostSwitchMap.put(switch_identifier);
	}*/
	
	protected Map<Long, SwitchPort> hostSwitchMap;
	
	public HostSwitchMap() {
		hostSwitchMap = new ConcurrentHashMap<Long, SwitchPort>();
	}
	
	protected SwitchPort getSwitchPort (Long host) {
		/*if (hostSwitchMap.containsKey(host)) {
			return hostSwitchMap.get(host);
		}
		else {
			return null;
		}*/
		return hostSwitchMap.get(host);
	}
	
	protected void putMap (Long source_mac, SwitchPort attach_point) {
		hostSwitchMap.put(source_mac, attach_point);
	}
	
	protected void updateMap (Long source_mac, SwitchPort cur_attach_point) {
		hostSwitchMap.put(source_mac, cur_attach_point);
	}

}
