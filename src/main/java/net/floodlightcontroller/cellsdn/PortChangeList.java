package net.floodlightcontroller.cellsdn;

import java.util.List;
import java.util.Vector;

import net.floodlightcontroller.devicemanager.SwitchPort;

public class PortChangeList {
	protected List<SwitchPort> port_change_list;
	
	public PortChangeList () {
		port_change_list = new Vector<SwitchPort>();
	}
	
	boolean containSwitchPort (SwitchPort switch_port) {
		return port_change_list.contains(switch_port);
	}
	
	void cleanSwitchPort (SwitchPort switch_port) {
		port_change_list.remove(switch_port);
	}

}
